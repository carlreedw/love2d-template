--[[
PROJECT: TEMPLATE
AUTHORS: 0
DATE: MM/DD/YYYY
DESC: 0
RUNW: "C:/reedcw1/dev/love.11.3/love.exe" "C:/reedcw1/dev/template"
RUNH: "C:/root/love2d/11.3/love.exe" "C:/root/dev/love2d/template"
AGENDA:
	all lib/mod/ext error handling
		error(..., 2)
		prints? debug.log when debug flag set?
		default behaviors and loud/quiet error reporting
	add camera lib? (zoom, pan, tilt, bound)
	practice networking (p2p, server-client, and internet/web) (consider enet or LuaSocket?)
	figure out screen layout helpers (is there actually a good way to handle this abstractly?)
--]]

function love.load()
	game = love.filesystem.load('game.lua')({
		run_tests = true,
		-- load_logging = true
	})
	
	-- enable quit on esc and debug.debug when press debug.debug_key (f10 default)
	debug.listen(true)
end

function love.update(dt)
	game:update(dt)
end

function love.draw()
	game:draw(dt)
end

