local state = {}

function state:init()
	self.timer = 0
	
	
end

function state:draw()
	local lg = love.graphics
	love.graphics.setColor(1, 1, 1)
	local sw, sh = lg.getDimensions()
	lg.circle('line', sw/2, sh/2, 50 + math.sin(self.timer * math.pi) * 25, 128)
end

function state:update(dt)
	self.timer = self.timer + dt
end

return state