local args = ...

local suite = {
	desc = "Tests for the function.lua extend module.",
	tests = {
		{
			desc = "function.memoize(fn) returns a wrapper function to fn where the results are cached for given args",
			run = function()
				local f = function(x) return x+1 end
				local g = fn.memoize(f)
				local a = g(1)
				assert(a == 2, "expected a == 2, a: " .. debug.tostring(a))
				local a = g(1)
				assert(a == 2, "expected a == 2, a: " .. debug.tostring(a))
			end
		},
		{
			desc = "function.time(fn, ...) returns execution time and return values of fn(...)",
			run = function()
				local f = function(x) return x+1 end
				local t, res = fn.time(f, 1)
				assert(type(t) == 'number', "expected time result to be a number, t: " .. debug.tostring(t))
				assert(res == 2, "expected result to be 2, res: " .. debug.tostring(res))
			end
		},
		{
			desc = "fn.fn(f, ...) returns a wrapped function that calls f(...), appending new args to end of ...",
			run = function()
				local f = function(...)
					local n = select("#", ...)
					local str = ''
					for i = 1, n do
						str = str .. select(i, ...)
					end
					return str
				end
				
				local g = fn.fn(f, 'a')
				local str = g('b', 'c')
				assert(str=='abc', "expected str=='abc', str: " .. debug.tostring(str))
			end
		},
		{
			desc = "fn.once(f, ...) returns a wrapped function that calls f(...), appending new args to end of ..., but only does so once",
			run = function()
				local f = function(...)
					local n = select("#", ...)
					local str = ''
					for i = 1, n do
						str = str .. select(i, ...)
					end
					return str
				end
				
				local g = fn.once(f, 'a')
				local str = g('b', 'c')
				str = str .. (g('d') or '')
				assert(str=='abc', "expected str=='abc', str: " .. debug.tostring(str))
			end
		},
		{
			desc = "fn.call(fn, ...) returns fn(...) if fn exists",
			run = function()
				local f = function(v) return v end
				assert(fn.call(f, true), "expected fn.call(f, true) to return true, result: " .. debug.tostring(fn.call(f, true)))
				assert(fn.call(g, true) == nil, "expected fn.call(g, true) == nil to return true, result: " .. debug.tostring(fn.call(g, true)))
			end
		}
	}
}

return suite