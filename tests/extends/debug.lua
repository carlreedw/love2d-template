local args = ...

local suite = {
	desc = "Tests for the debug.lua extend module.",
	tests = {
		{
			desc = "debug.trace(...) prints current filename and line number followed by args sep by space",
			run = function()
				local trace = debug.trace('hello', 1234)
				assert(trace == "tests/extends/debug.lua:9: hello 1234", "expected different trace value, trace: " .. tostring(trace))
			end
		}
		,{
			desc = "debug.hotswap(modname) reloads module, or nil and error message if hotswap failed",
			run = function()
				local ok, err = debug.hotswap('doesnt_exist')
				assert(not ok and err, "expected error when trying to hotswap non-existent module, ok: " .. tostring(ok))
			end
		}
	}
}

return suite