local args = ...

local suite = {
	desc = "Tests for the global.lua extend module.",
	tests = {
		{
			desc = "ripairs(t) iterates over the contiguous array portion of a table in reverse",
			run = function()
				local t = {'a', 'b', 'c', 'd', 'e', nil, 'f'}
				local str = ''
				for i, v in ripairs(t) do
					str = str .. v
					assert(i ~= 7, "ripairs iterating over non-contiguous array portion of table t -- i: " .. tostring(i))
				end
				assert(str == "edcba", "expected str == 'edcba' -- str: " .. debug.tostring(str))
			end
		}
	}
}

return suite