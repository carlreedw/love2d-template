local name = io.popen('hostname')
for line in name:lines() do
	print(line)
end
name:close()

function string.getSystemName()
	local systemname = "N/A"
	
	local name = io.popen('hostname')
	if name then
		systemname = name:read("*all")
		name:close()
	end
	
	return systemname
end

OR



function string.getSystemName()
	-- not good actually, because no hostname env var in linux
	return os.getenv(jit.os == "Windows" and "computername" or "hostname")
end